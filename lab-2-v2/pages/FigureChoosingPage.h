#pragma once
#include "core/ui.h"

namespace pages {
	class FigureChoosingPage {
		public:
			FigureChoosingPage();
			
			bool display(ui::WindowManager*);
	};
}