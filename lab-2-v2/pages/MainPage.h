#pragma once
#include "core/ui.h"

namespace pages {
	class MainPage {
		public:
			MainPage();
			
			bool display(ui::WindowManager*);
	};
}