#include "FigureChoosingPage.h"
#include "core/ui.h"
#include "components/WindowHeader/WindowHeader.h"
#include "components/Menu/Menu.h"

pages::FigureChoosingPage::FigureChoosingPage() {}

bool pages::FigureChoosingPage::display(ui::WindowManager* window_manager) {
	ui::WindowHeader("Choose figure").display();
	ui::Menu menu = {};
	
	menu.add("Rectangle", []() -> bool { return false; });
	menu.add("Circle", []() -> bool { return false; });
	menu.add("Back", [window_manager]() -> bool {
		window_manager->switch_to("main");
		return false;
	});
	
	return menu.display();
}