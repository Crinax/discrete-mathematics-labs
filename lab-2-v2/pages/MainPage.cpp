#include "MainPage.h"
#include "core/ui.h"
#include "components/Menu/Menu.h"
#include "components/WindowHeader/WindowHeader.h"

pages::MainPage::MainPage() {}

bool pages::MainPage::display(ui::WindowManager* window_manager) {
	ui::WindowHeader("Choose option").display();
	ui::Menu menu = {};
	
	menu.add("Enter figure", [window_manager]() -> bool {
		window_manager->switch_to("figure-choosing");
		return false;
	});
	
	menu.add("Check point", []() -> bool { return false; });
	menu.add("Exit", []() -> bool { return true; });
	
	return menu.display();
}