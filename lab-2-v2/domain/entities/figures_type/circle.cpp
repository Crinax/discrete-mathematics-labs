#include <stdio.h>
#include <cmath>
#include "circle.h"
#include "../figure.h"
#include "../point.h"

using domain::geometry::Figure;
using domain::geometry::FigureType;
using domain::geometry::Point;

domain::geometry::figure::Circle::Circle(
	Point coords,
	double radius
): Figure(coords, FigureType::Circle), _radius(radius) {};

double domain::geometry::figure::Circle::get_radius() {
	return this->_radius;
}

bool domain::geometry::figure::Circle::operator<<(Point point) {
	double length_to_center = sqrt(
		pow(this->_coords.get_x() - point.get_x(), 2) +
		pow(this->_coords.get_y() - point.get_y(), 2)
	);
	
	return length_to_center <= this->_radius;
}