#pragma once
#include "../figure.h"
#include "../point.h"

namespace domain {
	namespace geometry {
		namespace figure {
			class Rectangle : public Figure {
				public:
					Rectangle(Point, double, double);
					
					double get_width();
					double get_height();
					
					bool operator<<(Point point);
				
				protected:
					double _width;
					double _height;
			};
		}
	}
}
