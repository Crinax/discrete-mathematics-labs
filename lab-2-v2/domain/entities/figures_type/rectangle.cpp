#include "../figure.h"
#include "../point.h"
#include "rectangle.h"

using domain::geometry::Point;
using domain::geometry::Figure;
using domain::geometry::FigureType;

domain::geometry::figure::Rectangle::Rectangle(
	Point point,
	double width,
	double height
): Figure(
	point,
	FigureType::Rectangle
), _width(width), _height(height) { }

double domain::geometry::figure::Rectangle::get_width() {
	return this->_width;
}

double domain::geometry::figure::Rectangle::get_height() {
	return this->_height;
}

bool domain::geometry::figure::Rectangle::operator<<(Point point) {
	double half_width = this->_width / 2;
	double half_height = this->_height / 2;

	return (
		this->_coords.get_x() - half_width <= point.get_x() &&
		this->_coords.get_x() + half_width >= point.get_x()
	) && (
		this->_coords.get_x() - half_height <= point.get_y() &&
		this->_coords.get_x() + half_height >= point.get_y()
	);
}