#pragma once
#include "../figure.h"
#include "../point.h"

namespace domain {
	namespace geometry {
		namespace figure {
			class Circle : public Figure {
				public:
					Circle(Point, double);
				
					double get_radius();
					
					bool operator<<(Point point);
				
				private:
					double _radius;
			};
		}
	}
}