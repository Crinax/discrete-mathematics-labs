#pragma once

namespace domain {
	namespace geometry {
		class Point {
			public:
				Point(double, double);
				
				double get_x();
				double get_y();
				
			private:
				double _x;
				double _y;
		};
	}
}