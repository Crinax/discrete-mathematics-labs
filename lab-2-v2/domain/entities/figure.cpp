#include "figure.h"
#include "point.h"

domain::geometry::Figure::Figure(
	domain::geometry::Point coords,
	domain::geometry::FigureType type
): _coords(coords), _type(type) {}

domain::geometry::FigureType domain::geometry::Figure::get_type() {
	return this->_type;
}

domain::geometry::Point domain::geometry::Figure::get_coords() {
	return this->_coords;
}