#include "point.h"

domain::geometry::Point::Point(double x, double y): _x(x), _y(y) {}

double domain::geometry::Point::get_x() {
	return this->_x;
}

double domain::geometry::Point::get_y() {
	return this->_y;
}