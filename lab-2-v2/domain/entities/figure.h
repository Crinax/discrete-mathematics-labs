#pragma once
#include "point.h"

namespace domain {
	namespace geometry {
		enum struct FigureType {
			Unknown,
			Rectangle,
			Circle
		};
		
		class Figure {
			public:
				
				Figure(Point, FigureType);
				
				FigureType get_type();
				Point get_coords();
			
			protected:
				Point _coords;
				FigureType _type;
		};
	}
}