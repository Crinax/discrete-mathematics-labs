#include <iostream>
#include <functional>
#include "core/app.h"
#include "core/ui.h"
#include "components/WindowHeader/WindowHeader.h"
#include "components/Menu/Menu.h"
#include "domain/entities/point.h"
#include "domain/entities/figures_type/circle.h"
#include "domain/entities/figures_type/rectangle.h"
#include "pages/MainPage.h"
#include "pages/FigureChoosingPage.h"

class LabModule : public application::Module {
	void load_dependencies(ui::WindowManager* window_manager) {
		pages::MainPage main_page = {};
		pages::FigureChoosingPage figure_choosing_page = {};
		
		std::function<bool(ui::WindowManager*)> display_main_page = std::bind(
			&main_page.display,
			&main_page,
			std::placeholders::_1
		);
		
		std::function<bool(ui::WindowManager*)> display_figure_choosing_page = std::bind(
			&figure_choosing_page.display,
			&figure_choosing_page,
			std::placeholders::_1
		);
		
		window_manager->add(
			"main",
			display_main_page
		);
		
		window_manager->add(
			"figure-choosing",
			display_figure_choosing_page
		);
	}
};

int main() {
	application::App app = { };

	LabModule module = {};

	app.load_module(&module);
	
	return app.run();
}
