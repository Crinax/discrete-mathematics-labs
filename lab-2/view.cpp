#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <functional>
#include "console.h"
#include "view.h"

// ============ Component ============
view::components::Component::Component() {}

bool view::components::Component::display() {
	return false;
}	

// ============ Menu Component ============
view::components::Menu::Menu(): Component() {
	this->_menu_keys = {};
	this->_menu_handlers = {};
}

void view::components::Menu::add(std::string key, std::function<bool()> handler) {
	this->_menu_keys.push_back(key);
	this->_menu_handlers.push_back(handler);
}

void view::components::Menu::clear() {
	this->_menu_keys.clear();
	this->_menu_handlers.clear();
}

bool view::components::Menu::display() {
	int menu_size = this->_menu_keys.size();
	
	for (int i = 0; i < menu_size; i++) {
		printf("%d. %s\n", i + 1, this->_menu_keys[i].c_str());
	}
	
	printf("\nChoose menu: ");
	char input[1];
	gets(input);
	
	int choosed_menu = std::atoi(input);
	
	if (choosed_menu > 0 && choosed_menu <= menu_size) {
		bool handler_result = this->_menu_handlers[choosed_menu - 1]();
		
		return handler_result;
	}
	
	return false;
}

// ============ Window Manager Component ============
view::components::WindowManager::WindowManager(): Component() {
	this->_draw_functions = { };
	this->_current_window = 0;
}

void view::components::WindowManager::add(std::function<bool(view::components::WindowManager*)> draw_function) {
	this->_draw_functions.push_back(draw_function);

	if (this->_current_window == 0) {
		this->_current_window++;
	}
}

unsigned int view::components::WindowManager::count() {
	return this->_draw_functions.size();
}

bool view::components::WindowManager::switch_to(unsigned int window) {
	if (window > this->count()) {
		return false;
	}
	
	this->_current_window = window;
	return true;
}

unsigned int view::components::WindowManager::get_current_window() {
	return this->_current_window;
}

bool view::components::WindowManager::display() {
	if (this->_current_window == 0) {
		printf("Error: Windows was not added");
		
		return true;
	}
	
	return this->_draw_functions[this->_current_window - 1](this);
}

// ============ Window Header Component ============
view::components::WindowHeader::WindowHeader(std::string header):
	Component()
{
	this->_text = header;
}

bool view::components::WindowHeader::display() {
	printf("=== %s ===\n\n", this->_text.c_str());
	
	return false;
}

// ============ Error Message Component ============
view::components::ErrorMessage::ErrorMessage(std::string text):
	Component()
{
	this->_text = text;
}

bool view::components::ErrorMessage::display() {
	printf("\n===> Error: %s\n", this->_text.c_str());
	
	char input[1];
	gets(input);
	
	return false;
}

// ============ Input Component ============
view::components::InputComponent::InputComponent(std::function<bool(std::string)> handler):
	Component(),
	_handler(handler)
{ }

bool view::components::InputComponent::display() {
	char result[32];
	gets(result);
	
	return this->_handler(std::string(result));
}

// ============ Text Component ============
view::components::TextComponent::TextComponent(std::string text):
	Component(),
	_text(text)
{ }

void view::components::TextComponent::replace(std::string text) {
	this->_text = text;
}

bool view::components::TextComponent::display() {
	printf(this->_text.c_str());
	
	return false;
}

// ============ Combinator ============
view::components::Combinator::Combinator(
	std::vector<view::components::Component*> components
): _components(components) { };

void view::components::Combinator::replace(
	std::vector<view::components::Component*> components
)
{
	this->_components = components;
}

void view::components::Combinator::add(
	view::components::Component* component
)
{
	this->_components.push_back(component);
}

// ============ Error Diffuser ============
view::components::ErrorDiffuser::ErrorDiffuser(
	std::vector<view::components::Component*> components
): Combinator(components) { }

bool view::components::ErrorDiffuser::display() {
	bool is_repeat = true;
	bool need_to_rerender = false;
	int components_count = this->_components.size();
	
	while (is_repeat) {
		int i = 0;
		for (; (i < components_count) && !need_to_rerender; i++) {
			try {
				this->_components[i]->display();
			} catch(...) {
				console::clear_lines_before(i);
				need_to_rerender = true;
			}
		}
		
		if (need_to_rerender) {
			need_to_rerender = false;
			continue;
		}
		
		is_repeat = false;
	}
	
	return false;
}

// ============ Input Reader Component ============
view::components::InputReaderComponent::InputReaderComponent(double& input):
	_input(input)
{}

bool view::components::InputReaderComponent::display() {
	view::components::InputComponent input_field = {
		[=](std::string result) mutable -> bool {
			double value = std::stod(result);
			this->_input = value;

			return false;
		} 
	};
	
	return false;
}