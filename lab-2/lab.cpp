#include "lab.h"
#include "view.h"
#include "console.h"

application::Lab::Lab(
	view::components::WindowManager* window_manager,
	std::function<bool(unsigned int)> switch_to
): _window_manager(window_manager), _switch_to(switch_to) {	
	std::function<bool(view::components::WindowManager*)> app_menu_func = std::bind(
		&this->show_app_menu,
		this,
		std::placeholders::_1
	);
	
	std::function<bool(view::components::WindowManager*)> figure_choose_menu_func = std::bind(
		&this->show_choose_figure_menu,
		this,
		std::placeholders::_1
	);
	
	std::function<bool(view::components::WindowManager*)> rectangle_input_func = std::bind(
		&this->show_input_rectangle,
		this,
		std::placeholders::_1
	);
	
	this->_window_manager->add(app_menu_func);
	this->_window_manager->add(figure_choose_menu_func);
	this->_window_manager->add(rectangle_input_func);
}

bool application::Lab::show_app_menu(view::components::WindowManager* window_manager) {
	view::components::WindowHeader header = { "Select a menu item" };
	header.display();
	
	view::components::Menu menu = { };
	
	menu.add("Enter figures", [=]() -> bool { return this->_switch_to(2); });
	menu.add("Check whether a point belongs to a set", [=]() -> bool { return this->_switch_to(4); });
	menu.add("Exit", []() -> bool { return true; });
	
	bool display_result = menu.display();
	return display_result;
}

bool application::Lab::show_choose_figure_menu(view::components::WindowManager* window_manager) {
	view::components::WindowHeader header = { "Select a figure to enter" };
	header.display();
	
	view::components::Menu menu = { };
	
	menu.add("Rectangle", [=]() -> bool { return this->_switch_to(3); });
	menu.add("Circle", []() -> bool { return false; });
	menu.add("Back", [=]() -> bool { return this->_switch_to(1); });
	menu.add("Exit", []() -> bool { return true; });
	
	bool display_result = menu.display();
	return display_result;
}

bool application::Lab::show_input_rectangle(view::components::WindowManager* window_manager) {
	double x_coord;
	double y_coord;
	double width;
	double height;
	
	view::components::WindowHeader header = { "Enter rectangle parameters" };
	view::components::TextComponent x_coord_input_placeholder = { "Enter X position: " };
	view::components::InputComponent x_coord_input = {
		[&x_coord](std::string result) mutable -> bool {
			double value = std::stod(result);
			x_coord = value;

			return false;
		}
	};
	view::components::TextComponent y_coord_input_placeholder = { "Enter Y position: " };
	view::components::InputComponent y_coord_input = {
		[&y_coord](std::string result) mutable -> bool {
			double value = std::stod(result);
			y_coord = value;

			return false;
		}
	};
	view::components::ErrorDiffuser diffuser({
		&x_coord_input_placeholder,
		&x_coord_input,
	});
	
	header.display();
	diffuser.display();
	diffuser.replace({ &y_coord_input_placeholder, &y_coord_input });
	diffuser.display();
	
	printf("%lf", x_coord);
		
	return false;
}