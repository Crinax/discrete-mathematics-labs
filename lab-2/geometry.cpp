#include "geometry.h"

// ============ Point ============
geometry::Point::Point(double x, double y): _x(x), _y(y) {}

double geometry::Point::get_x() {
	return this->_x;
}

double geometry::Point::get_y() {
	return this->_y;
}

// ============ Figure ============
geometry::Figure::Figure(Point point, FigureType type): _point(point), _type(type) {}

geometry::Point geometry::Figure::get_coords() {
	return this->_point;
}

geometry::FigureType geometry::Figure::get_type() {
	return this->_type;
}

// ============ Rectangle ============
geometry::Rectangle::Rectangle(Point point, double width, double height):
	Figure(point, geometry::FigureType::Rectangle),
	_width(width),
	_height(height)
{}

double geometry::Rectangle::get_width() {
	return this->_width;
}

double geometry::Rectangle::get_height() {
	return this->_height;
}

// ============ Circle ============
geometry::Circle::Circle(Point point, double radius):
	Figure(point, geometry::FigureType::Circle),
	_radius(radius)
{}

double geometry::Circle::get_radius() {
	return this->_radius;
}