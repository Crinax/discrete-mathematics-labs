#pragma once
#include <functional>

namespace console {
	class LoopManager {
		public:
			LoopManager();
			
			bool get_status();
			void stop();
			void run();
		
		private:
			bool _is_run;
	};
	
	class Manager {
		public:
			Manager();
			
			void run_loop(std::function<void(LoopManager*)>);
	};
	
	void clear();
	void clear_current_line();
	void clear_lines_before(int);
}