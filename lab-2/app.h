#pragma once
#include "console.h"
#include "view.h"

namespace application {
	class App {
		public:
			App();
			
			int run();
			void initialize_loop(view::components::WindowManager*, console::LoopManager*);
			bool switch_window(view::components::WindowManager*, unsigned int menu);
		
		private:
			console::Manager _console_manager;
	};
	
	
}