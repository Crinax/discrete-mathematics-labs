#pragma once

namespace geometry {
	class Point {
		public:
			Point(double, double);
			
			double get_x();
			double get_y();

		private:
			double _x;
			double _y;
	};
	
	enum struct FigureType {
		Unknown,
		Rectangle,
		Circle,
	};
	
	class Figure {
		public:
			Figure(Point, FigureType type);
			
			FigureType get_type();
			Point get_coords();
			
		protected:
			Point _point;
			FigureType _type;
	};
	
	class Rectangle : public Figure {
		public:
			Rectangle(Point, double, double);
			
			double get_width();
			double get_height();
		
		protected:
			double _width;
			double _height;
	};
	
	class Circle : public Figure {
		public:
			Circle(Point, double);
			
			double get_radius();
			
		protected:
			double _radius;
	};
}