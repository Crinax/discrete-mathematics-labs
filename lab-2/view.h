#pragma once
#include <string>
#include <vector>
#include <functional>
#include "console.h"

namespace view {
	namespace components {
		class Component {
			public:
				Component();
				
				virtual bool display();
		};
		
		class Menu : public Component {
			public:
				Menu();
				
				void add(std::string, std::function<bool()>);
				void clear();
				bool display();

			private:
				std::vector<std::string> _menu_keys;
				std::vector<std::function<bool()>> _menu_handlers;		
		};
		
		class WindowManager : public Component {
			public:
				WindowManager();
				
				void add(std::function<bool(WindowManager*)>);
				bool display();
				unsigned int count();
				bool switch_to(unsigned int window);
				unsigned int get_current_window();

			private:
				std::vector<std::function<bool(WindowManager*)>> _draw_functions;
				unsigned int _current_window;
		};
		
		class WindowHeader : public Component {
			public:
				WindowHeader(std::string);
				
				bool display();
			
			private:
				std::string _text;
		};
		
		class ErrorMessage : public Component {
			public:
				ErrorMessage(std::string);
				
				bool display();
			
			private:
				std::string _text;
		};
		
		class InputComponent : public Component {
			public:
				InputComponent(std::function<bool(std::string)>);
				
				bool display();
				
			private:
				std::string _text;
				std::function<bool(std::string)> _handler;
		};
		
		class TextComponent : public Component {
			public:
				TextComponent(std::string);
				
				bool display();
				void replace(std::string);

			private:
				std::string _text;
		};
		
		class Combinator : public Component {
			public:
				Combinator(std::vector<Component*>);
				
				virtual bool display() {
					int components_count = this->_components.size();
				
					for (int i = 0; i < components_count; i++) {
						this->_components[i]->display();
					}
					
					return false;
				};
				
				void replace(std::vector<Component*>);
				void add(Component*);
				
			protected:
				std::vector<Component*> _components;
		};
		
		class ErrorDiffuser : public Combinator {
			public:
				ErrorDiffuser(std::vector<Component*>);
				
				bool display();
		};
		
		class InputReaderComponent : public Component {
			public:
				InputReaderComponent(double&);
				
				bool display();
			
			private:
				double& _input;
		};
	}
}