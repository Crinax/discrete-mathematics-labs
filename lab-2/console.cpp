#include <stdlib.h>
#include <functional>
#include "console.h"

// ============ Console Loop ============
console::LoopManager::LoopManager(): _is_run(false) {}

bool console::LoopManager::get_status() {
	return this->_is_run;
}

void console::LoopManager::stop() {
	this->_is_run = false;
}

void console::LoopManager::run() {
	this->_is_run = true;
}

// ============ Console Manager ============
console::Manager::Manager() {}

void console::Manager::run_loop(std::function<void(LoopManager*)> fn) {
	LoopManager loop_manager = {};
	
	loop_manager.run();
	while (loop_manager.get_status()) {
		fn(&loop_manager);

		if (loop_manager.get_status()) {
			console::clear();
		}
	}
}

// ============ clear ============
void console::clear() {
	system("cls");
}

void console::clear_current_line() {
	printf("\x1b[1F");
	printf("\x1b[2K");
}

void console::clear_lines_before(int count) {
	for (int i = count; i > 0; i--) {
		console::clear_current_line();
	}
}