#pragma once
#include <functional>
#include "view.h"

namespace application {
	class Lab {
		public:
			Lab(view::components::WindowManager*, std::function<bool(unsigned int)>);
			
			bool show_app_menu(view::components::WindowManager*);
			bool show_choose_figure_menu(view::components::WindowManager*);
			bool show_input_rectangle(view::components::WindowManager*);
			
		private:
			view::components::WindowManager* _window_manager;
			std::function<bool(unsigned int)> _switch_to;
	};	
}