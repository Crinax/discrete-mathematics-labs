#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstdio>
#include "geometry.h"
#include "console.h"
#include "app.h"
#include "view.h"
#include "lab.h"

application::App::App() {
	this->_console_manager = {};
}

bool application::App::switch_window(
	view::components::WindowManager* window_manager,
	unsigned int menu
)
{
	bool is_switched = window_manager->switch_to(menu);
	
	if (!is_switched) {
		view::components::ErrorMessage error = {
			"Window " + std::to_string(menu) + " not exists"
		};
		
		error.display();
	}
	
	return false;
}

void application::App::initialize_loop(
	view::components::WindowManager* window_manager,
	console::LoopManager *loop_manager
)
{
	bool display_result = window_manager->display();
	if (display_result) {
		loop_manager->stop();
	}
}

int application::App::run() {
	view::components::WindowManager window_manager = { };
	
	std::function<bool(unsigned int)> switch_to = std::bind(
		&this->switch_window,
		this,
		&window_manager,
		std::placeholders::_1
	);

	application::Lab lab = { &window_manager, switch_to };
	
	auto fp = std::bind(&this->initialize_loop, this, &window_manager, std::placeholders::_1);
	this->_console_manager.run_loop(fp);
	
	return 0;
}

