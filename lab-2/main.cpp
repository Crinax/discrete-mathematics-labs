#include <memory>
#include "app.h"

#define ENTRY_POINT(__APP__)             \
	int main() {                         \
		std::unique_ptr<__APP__> app =   \
			std::make_unique<__APP__>(); \
		return app->run();               \
	}

ENTRY_POINT(application::App);