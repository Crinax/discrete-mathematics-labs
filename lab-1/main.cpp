#include <iostream>
#include <stdlib.h>
#include <string.h>

// Предикат для 3-его и 4-ого вариантов
bool predicate_variant_3_4(
	bool a,
	bool b,
	bool c,
	bool d,
	bool e
) {
	return ((a && b && !c) || (!d && e));
}

// Предикат для 2-ого варианта
bool predicate_variant_2(
	bool a,
	bool b,
	bool c,
	bool d,
	bool e
) {
	return ((!a && b) || c || (d && !e));
}

// Функция для вычисления и записи предиката
void write_predicate(int column, bool row[], bool (*func)(bool, bool, bool, bool, bool)) {
	row[column] = (*func)(row[0], row[1], row[2], row[3], row[4]);
}

// Функция для заполнения текущей строки числом в двоичной системе счисления
void fill_row(int number, int columns, bool arr[]) {
	int n = number;
	int i = 0;
	
	while (n != 0) {
		if (i >= columns) return;
		arr[columns - i - 1] = (bool)(n % 2);
		n = n / 2;
		i++;
	}
	
	if (i < columns) {
		for (i; i < columns; i++) {
			arr[columns - i - 1] = false;
		}
	}
}

// Функция строящая СДНФ для одной строки
void fdnf_for_row(bool row[], char s[], char vars[], int length, int& last_pos) {
	s[last_pos++] = ' ';
	s[last_pos++] = '(';

	for (int i = 0; i < length; i++) {
		if (row[i]) {
			s[last_pos++] = vars[i];
		} else {
			s[last_pos++] = '!';
			s[last_pos++] = vars[i];
		}
		
		if (i != length - 1) {
			s[last_pos++] = ' ';
			s[last_pos++] = '&';
			s[last_pos++] = '&';
			s[last_pos++] = ' ';
		}
	}
	
	s[last_pos++] = ')';
}

// Функция строящая СКНФ для одной строки
void fcnf_for_row(bool row[], char s[], char vars[], int length, int& last_pos) {
	s[last_pos++] = ' ';
	s[last_pos++] = '(';

	for (int i = 0; i < length; i++) {
		if (!row[i]) {
			s[last_pos++] = vars[i];
		} else {
			s[last_pos++] = '!';
			s[last_pos++] = vars[i];
		}
		
		if (i != length - 1) {
			s[last_pos++] = ' ';
			s[last_pos++] = '|';
			s[last_pos++] = '|';
			s[last_pos++] = ' ';
		}
	}
	
	s[last_pos++] = ')';
}

int main() {
	setlocale(LC_ALL, "Russian");
	
	const int SIZE = 32;
	const int DEEP = 2;
	const int COLUMNS = 6;
	char TABLES[] = "abcdef";
	
	bool table[SIZE][COLUMNS];
	
	printf("a b c d e f\n");
	
	for (int row = 0; row < SIZE; row++) {
		fill_row(row, COLUMNS - 1, table[row]);
		write_predicate(COLUMNS - 1, table[row], *predicate_variant_3_4);
		//       a  b  c  d  e  f
		printf("%d %d %d %d %d %d\n",
			table[row][0],
			table[row][1],
			table[row][2],
			table[row][3],
			table[row][4],
			table[row][5]
		);
	}
	
	char fdnf[1000];
	char fcnf[1000];
	int last_position_fdnf = 0;
	int last_position_fcnf = 0;
	fdnf[0] = 0;
	fcnf[0] = 0;
	
	for (int row = 0; row < SIZE; row++) {
		if (table[row][5]) {
			if (last_position_fdnf != 0) {
				fdnf[last_position_fdnf++] = ' ';
				fdnf[last_position_fdnf++] = '|';
				fdnf[last_position_fdnf++] = '|';
			}

			fdnf_for_row(table[row], fdnf, TABLES, COLUMNS - 1, last_position_fdnf);
		} else {
			if (last_position_fcnf != 0) {
				fcnf[last_position_fcnf++] = ' ';
				fcnf[last_position_fcnf++] = '&';
				fcnf[last_position_fcnf++] = '&';
			}

			fcnf_for_row(table[row], fcnf, TABLES, COLUMNS - 1, last_position_fcnf);
		}
	}
	
	printf("\nFDNF(f) =%s\n\n", fdnf);
	printf("FCNF(f) =%s\n\n", fcnf);
	
	system("pause");
	
	printf("\x1b[1F");
	printf("\x1b[2K");
	printf("\x1b[1F");
	printf("\x1b[2K");
	
	system("pause");
	
	return 0;
}